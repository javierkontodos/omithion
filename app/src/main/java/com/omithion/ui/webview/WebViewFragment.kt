package com.omithion.ui.webview

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.SslErrorHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.omithion.R
import com.omithion.databinding.FragmentWebViewBinding
import com.omithion.ui.commons.visibleOrNot
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WebViewFragment : Fragment() {
    private lateinit var binding: FragmentWebViewBinding
    private val args: WebViewFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_web_view, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnBack.setOnClickListener {
            if (binding.webView.canGoBack()) {
                binding.webView.goBack()
            } else {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }

        activity?.onBackPressedDispatcher?.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (binding.webView.canGoBack()) {
                        binding.webView.goBack()
                    } else {
                        super.remove()
                        findNavController().popBackStack()
                    }
                }
            }
        )

        configWebView()
    }

    private fun configWebView() {
        binding.webView.apply {
            this.settings.javaScriptEnabled = true
            this.settings.domStorageEnabled = true
            this.webViewClient = object : WebViewClient() {
                override fun onReceivedSslError(
                    view: WebView?,
                    handler: SslErrorHandler?,
                    error: SslError?
                ) {
                    AlertDialog.Builder(requireContext())
                        .setMessage("fffff")
                        .setPositiveButton("Contigual") { _, _ -> handler!!.proceed() }
                        .setNeutralButton("Cancelar") { _, _ -> handler!!.cancel() }
                        .show()
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    //binding.webView.loadUrl("javascript:(function(){ document.body.style.paddingBottom = '80px'})();")
                    if (url?.startsWith("mailto:") == false || url?.startsWith("tel:") == false) {
                        binding.webView.visibleOrNot(true)
                    }
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    if (url.startsWith("mailto:") || url.startsWith("tel:")) {
                        val intent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(url)
                        )
                        view.context.startActivity(intent)
                        return true
                    }
                    if (url.endsWith(".pdf")) {
                        binding.webView.loadUrl("https://docs.google.com/gview?embedded=true&url=$url")
                    }
                    return false
                }
            }
            this.loadUrl(args.webUrl)
        }
    }
}