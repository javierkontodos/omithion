package com.omithion.ui.splash

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.omithion.R
import com.omithion.databinding.ActivitySplashBinding
import com.omithion.ui.MainActivity
import com.omithion.ui.commons.visibleOrNot
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    private var notificationUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //For test Light Mode
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        val data: Uri? = intent?.data
        if (data != null) {
            notificationUrl = data.toString()
        }

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_zoom_in)
        val anim2 = AnimationUtils.loadAnimation(applicationContext, R.anim.anim_bounce)
        anim.setAnimationListener(object: Animation.AnimationListener{
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                binding.ivTitle.visibleOrNot(true)
                binding.ivTitle.startAnimation(anim2)
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        anim2.setAnimationListener(object: Animation.AnimationListener{
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                lifecycleScope.launch {
                    delay(250)
                    navigateToMainActivity()
                }
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })

        binding.ivLogo.startAnimation(anim)
    }

    private fun navigateToMainActivity() {
        startActivity(MainActivity.newIntent(this, notificationUrl))
        notificationUrl = null
        finish()

    }
}