package com.omithion.ui.about.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.webkit.URLUtil
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.omithion.R
import com.omithion.databinding.FragmentAboutDetailsBinding
import com.omithion.domain.About
import com.omithion.domain.AboutType
import com.omithion.ui.about.AboutViewModel
import com.omithion.ui.about.adapter.LinksAdapter
import com.omithion.ui.commons.ItemOnClickListener
import timber.log.Timber
import java.lang.Exception

class AboutDetailsFragment : Fragment() {
    private lateinit var binding: FragmentAboutDetailsBinding
    private val viewModel: AboutViewModel by activityViewModels()
    private val args: AboutDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_about_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val position = args.position
        loadAboutDetails(viewModel.aboutList?.get(position))
    }

    private fun loadAboutDetails(about: About?) {
        about?.let {
            binding.about = about
            with(binding.rvLinks){
                layoutManager = LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)
                adapter =
                    LinksAdapter(about.links, object : ItemOnClickListener {
                        override fun onClickButton(position: Int, type: AboutType?, url: String?) {
                            if (position < 0){
                                manageLinkNavigation(type, url)
                            }
                        }
                    })
            }

            val anim = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_bounce)
            binding.tvName.startAnimation(anim)

            val anim2 = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_fade_in)
            binding.ivAboutDetail.startAnimation(anim2)
            binding.tvTitle.startAnimation(anim2)
            binding.tvContent.startAnimation(anim2)

        }
    }

    private fun manageLinkNavigation(type: AboutType?, url: String?) {
        when(type){
            AboutType.Linkedin,
            AboutType.Instagram,
            AboutType.Twitter,
            AboutType.Telegram,
            AboutType.Email -> navigateOut(url)
            AboutType.Website,
            AboutType.Other -> navigateToWebViewFragment(url)
        }
    }

    private fun navigateOut(url: String?) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }catch (e: Exception){
            Timber.e(e)
        }
    }

    private fun navigateToWebViewFragment(url: String?) {
        if (url != null && URLUtil.isValidUrl(url)){
            val action = AboutDetailsFragmentDirections.actionAboutDetailsFragmentToWebViewFragment(url)
            findNavController().navigate(action)
        }
    }
}