package com.omithion.ui.about.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.omithion.R
import com.omithion.databinding.ItemAboutBinding
import com.omithion.domain.About
import com.omithion.domain.AboutType
import com.omithion.ui.commons.ItemOnClickListener
import kotlin.reflect.typeOf

class AboutAdapter (
    private val aboutList: List<About>,
    private val listener: ItemOnClickListener
) : RecyclerView.Adapter<AboutViewHolder>() {

    override fun getItemCount(): Int {
        return aboutList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutViewHolder =
        AboutViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_about,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: AboutViewHolder, position: Int) {
        holder.bind(aboutList[position], listener)
    }
}

class AboutViewHolder(val binding: ItemAboutBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(
        about: About,
        listener: ItemOnClickListener
    ) {
        binding.about = about
        binding.btnMore.setOnClickListener {
            listener.onClickButton(layoutPosition)
        }

        with(binding.rvLinks){
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter =
                LinksAdapter(about.links, object : ItemOnClickListener {
                    override fun onClickButton(position: Int, type: AboutType?, url: String?) {
                        listener.onClickButton(position, type, url)
                    }
                })
        }

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in)
        binding.ivAbout.startAnimation(anim)

        val anim2 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in_250)
        binding.cvAbout.startAnimation(anim2)

        val anim3 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_zoom_in)
        binding.btnMore.startAnimation(anim3)
    }
}