package com.omithion.ui.about.adapter

import android.os.Handler
import android.os.Looper.getMainLooper
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.omithion.R
import com.omithion.databinding.ItemLinkBinding
import com.omithion.domain.AboutLink
import com.omithion.domain.AboutType
import com.omithion.ui.commons.ItemOnClickListener

class LinksAdapter(
    private val links: List<AboutLink>,
    private val listener: ItemOnClickListener
) : RecyclerView.Adapter<LinksViewHolder>() {

    override fun getItemCount(): Int {
        return links.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinksViewHolder =
        LinksViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_link,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: LinksViewHolder, position: Int) {
        holder.bind(links[position], listener)
    }
}

class LinksViewHolder(val binding: ItemLinkBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(
        aboutLink: AboutLink,
        listener: ItemOnClickListener
    ) {
        binding.aboutLink = aboutLink
        binding.linkResource = when(aboutLink.type){
            AboutType.Linkedin -> R.drawable.ic_linkedin
            AboutType.Instagram -> R.drawable.ic_instagram
            AboutType.Twitter -> R.drawable.ic_twitter
            AboutType.Telegram -> R.drawable.ic_telegram
            AboutType.Email -> R.drawable.ic_email
            AboutType.Website -> R.drawable.ic_website
            AboutType.Other -> R.drawable.ic_other
        }
        binding.ivLink.setOnClickListener {
            listener.onClickButton(-1, type = aboutLink.type, url = aboutLink.url)
        }

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in)
        binding.ivLink.startAnimation(anim)
    }
}