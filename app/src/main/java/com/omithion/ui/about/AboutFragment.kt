package com.omithion.ui.about

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.omithion.R
import com.omithion.databinding.DialogCustomAlertBinding
import com.omithion.databinding.FragmentAboutBinding
import com.omithion.domain.About
import com.omithion.domain.AboutType
import com.omithion.ui.about.adapter.AboutAdapter
import com.omithion.ui.about.details.AboutDetailsFragmentDirections
import com.omithion.ui.commons.ItemOnClickListener
import com.omithion.ui.commons.visibleOrGone
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.lang.Exception

@AndroidEntryPoint
class AboutFragment : Fragment() {
    private lateinit var binding: FragmentAboutBinding
    private val viewModel: AboutViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.model.observe(viewLifecycleOwner) {
            when (it) {
                is AboutViewModel.UiModel.SuccessAboutContent -> showAboutContent(it.aboutList)
                is AboutViewModel.UiModel.Failure -> showFailureError()
                is AboutViewModel.UiModel.Loading -> loading()
            }
        }

        viewModel.aboutList?.let {
            showAboutContent(it)
        }?: run {
            viewModel.getAboutContent()
        }
    }

    private fun loading() {
        Timber.d("Loading...")
        binding.loading.visibleOrGone(true)
    }

    private fun showFailureError() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity?.layoutInflater
        val dialogBinding: DialogCustomAlertBinding =
            DataBindingUtil.inflate(
                inflater!!,
                R.layout.dialog_custom_alert,
                null,
                false
            )
        builder.setView(dialogBinding.root)
        val alert: AlertDialog = builder.create()
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert.window?.setGravity(Gravity.CENTER)
        dialogBinding.btnClose.setOnClickListener {
            alert.dismiss()
        }
        dialogBinding.lyErrorDialog.visibleOrGone(true)
        alert.show()
    }

    private fun showAboutContent(aboutList: List<About>) {
        binding.loading.visibleOrGone(false)
        with(binding.rvProjects) {
            layoutManager = LinearLayoutManager(activity)
            adapter =
                AboutAdapter(aboutList, object : ItemOnClickListener {
                    override fun onClickButton(position: Int, type: AboutType?, url: String?) {
                        if (position >= 0){
                            navigateToAboutDetails(position)
                        }else{
                            manageLinkNavigation(type, url)
                        }
                    }
                })
        }
    }

    private fun manageLinkNavigation(type: AboutType?, url: String?) {
        when(type){
            AboutType.Linkedin,
            AboutType.Instagram,
            AboutType.Twitter,
            AboutType.Telegram,
            AboutType.Email -> navigateOut(url)
            AboutType.Website,
            AboutType.Other -> navigateToWebViewFragment(url)
        }
    }

    private fun navigateOut(url: String?) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }catch (e: Exception){
            Timber.e(e)
        }
    }

    private fun navigateToWebViewFragment(url: String?) {
        if (url != null && URLUtil.isValidUrl(url)){
            val action = AboutFragmentDirections.actionAboutFragmentToWebViewFragment(url)
            findNavController().navigate(action)
        }
    }

    private fun navigateToAboutDetails(position: Int) {
        val action = AboutFragmentDirections.actionAboutFragmentToAboutDetailsFragment(position)
        findNavController().navigate(action)
    }
}