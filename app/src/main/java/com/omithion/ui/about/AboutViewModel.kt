package com.omithion.ui.about

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.omithion.data.usecases.GetAboutUseCase
import com.omithion.domain.About
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AboutViewModel @Inject constructor(
    private val getAboutUseCase: GetAboutUseCase
) : ViewModel() {
    private var _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel> = _model
    var aboutList: List<About>? = null

    fun getAboutContent() {
        _model.value = UiModel.Loading
        viewModelScope.launch {
            getAboutUseCase.invoke()?.let {
                handleSuccess(it)
            } ?: handleFailure()
        }
    }

    private fun handleFailure() {
        _model.value = UiModel.Failure
    }

    private fun handleSuccess(abouts: List<About>) {
        this.aboutList = abouts
        _model.value = UiModel.SuccessAboutContent(abouts)
    }

    sealed class UiModel {
        object Loading : UiModel()
        object Failure : UiModel()
        data class SuccessAboutContent(val aboutList: List<About>) : UiModel()
    }
}