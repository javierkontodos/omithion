package com.omithion.ui.commons

import com.omithion.domain.AboutType

interface ItemOnClickListener {
    fun onClickButton(position: Int, type: AboutType? = null, url: String? = null)
}