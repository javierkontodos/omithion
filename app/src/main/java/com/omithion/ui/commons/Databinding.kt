package com.omithion.ui.commons

import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.omithion.R

@BindingAdapter("visibleOrGone")
fun View.visibleOrGone(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("visibleOrNot")
fun View.visibleOrNot(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.INVISIBLE
}

@BindingAdapter("loadImageUrl")
fun ImageView.loadImageUrl(url: String) {
    if (url.isNotEmpty() && url.isNotBlank()) {
        Glide.with(this)
            .load(url)
            .placeholder(R.drawable.logo)
            .error(R.drawable.logo)
            .into(this)
    }
}

@BindingAdapter("loadRoundImageUrl")
fun ImageView.loadRoundImageUrl(url: String) {
    Glide.with(this)
        .load(url)
        .placeholder(R.drawable.logo)
        .error(R.drawable.logo)
        .apply(RequestOptions().circleCrop())
        .into(this)
}

@BindingAdapter("loadImageResource")
fun ImageView.loadImageResource(imgResource: Int) {
    Glide.with(this)
        .load(imgResource)
        .placeholder(R.drawable.logo)
        .error(R.drawable.logo)
        .into(this)
}

@BindingAdapter("enableButton")
fun Button.enableButton(isEnable: Boolean) {
    this.setTextColor(context.resources.getColor(R.color.white))
    if (isEnable) {
        this.setBackgroundColor(context.resources.getColor(R.color.red))
        this.isEnabled = true
    } else {
        this.setBackgroundColor(context.resources.getColor(R.color.gray))
        this.isEnabled = false
    }
}
