package com.omithion.ui.projects.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.omithion.R
import com.omithion.databinding.FragmentProjectDetailsBinding
import com.omithion.domain.Project
import com.omithion.ui.projects.ProjectsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProjectDetailsFragment : Fragment() {
    private lateinit var binding: FragmentProjectDetailsBinding
    private val viewModel: ProjectsViewModel by activityViewModels()
    private val args: ProjectDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_project_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val position = args.position
        loadProjectDetails(viewModel.projectsList?.get(position))
    }

    private fun loadProjectDetails(project: Project?) {
        project?.let {
            binding.project = it
            binding.btnOpen.setOnClickListener {
                navigateToWebView(project.url)
            }
            val anim = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_bounce)
            binding.tvTitle.startAnimation(anim)

            val anim2 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in)
            binding.ivProjectDetail.startAnimation(anim2)
            binding.tvContent.startAnimation(anim2)

            val anim3 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_zoom_in)
            binding.btnOpen.startAnimation(anim3)
        }
    }

    private fun navigateToWebView(url: String) {
        val action = ProjectDetailsFragmentDirections.actionProjectDetailsFragmentToWebViewFragment(url)
        findNavController().navigate(action)
    }
}