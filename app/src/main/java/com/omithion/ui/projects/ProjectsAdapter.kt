package com.omithion.ui.projects

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.omithion.R
import com.omithion.databinding.ItemProjectBinding
import com.omithion.domain.Project
import com.omithion.ui.commons.ItemOnClickListener

class ProjectsAdapter (
    private val projects: List<Project>,
    private val listener: ItemOnClickListener
) : RecyclerView.Adapter<ProjectsViewHolder>() {

    override fun getItemCount(): Int {
        return projects.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectsViewHolder =
        ProjectsViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_project,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ProjectsViewHolder, position: Int) {
        holder.bind(projects[position], listener)
    }
}

class ProjectsViewHolder(val binding: ItemProjectBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(
        project: Project,
        listener: ItemOnClickListener
    ) {
        binding.project = project

        binding.btnMore.setOnClickListener {
            listener.onClickButton(layoutPosition)
        }
        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in_250)
        binding.cvProject.startAnimation(anim)

        val anim2 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_zoom_in)
        binding.btnMore.startAnimation(anim2)
    }
}