package com.omithion.ui.projects

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.omithion.data.usecases.GetProjectsUseCase
import com.omithion.domain.Project
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProjectsViewModel @Inject constructor(
    private val getProjectsUseCase: GetProjectsUseCase
) : ViewModel() {
    private var _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel> = _model
    var projectsList: List<Project>? = null

    fun getProjects() {
        _model.value = UiModel.Loading
        viewModelScope.launch {
            getProjectsUseCase.invoke()?.let {
                handleSuccess(it)
            } ?: handleFailure()
        }
    }

    private fun handleFailure() {
        _model.value = UiModel.Failure
    }

    private fun handleSuccess(projects: List<Project>) {
        this.projectsList = projects
        _model.value = UiModel.SuccessProjects(projects)
    }

    sealed class UiModel {
        object Loading : UiModel()
        object Failure : UiModel()
        data class SuccessProjects(val projectList: List<Project>) : UiModel()
    }
}