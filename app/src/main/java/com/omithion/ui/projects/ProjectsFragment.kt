package com.omithion.ui.projects

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.omithion.R
import com.omithion.databinding.DialogCustomAlertBinding
import com.omithion.databinding.FragmentProjectsBinding
import com.omithion.domain.AboutType
import com.omithion.domain.Project
import com.omithion.ui.commons.ItemOnClickListener
import com.omithion.ui.commons.visibleOrGone
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class ProjectsFragment : Fragment() {
    private lateinit var binding: FragmentProjectsBinding
    private val viewModel: ProjectsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_projects, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.model.observe(viewLifecycleOwner) {
            when (it) {
                is ProjectsViewModel.UiModel.SuccessProjects -> showProjects(it.projectList)
                is ProjectsViewModel.UiModel.Failure -> showFailureError()
                is ProjectsViewModel.UiModel.Loading -> loading()
            }
        }

        viewModel.projectsList?.let {
            showProjects(it)
        }?: run {
            viewModel.getProjects()
        }
    }

    private fun loading() {
        Timber.d("Loading...")
        binding.loading.visibleOrGone(true)
    }

    private fun showFailureError() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity?.layoutInflater
        val dialogBinding: DialogCustomAlertBinding =
            DataBindingUtil.inflate(
                inflater!!,
                R.layout.dialog_custom_alert,
                null,
                false
            )
        builder.setView(dialogBinding.root)
        val alert: AlertDialog = builder.create()
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert.window?.setGravity(Gravity.CENTER)
        dialogBinding.btnClose.setOnClickListener {
            alert.dismiss()
        }
        dialogBinding.lyErrorDialog.visibleOrGone(true)
        alert.show()
    }

    private fun showProjects(projects: List<Project>) {
        binding.loading.visibleOrGone(false)
        with(binding.rvProjects) {
            layoutManager = LinearLayoutManager(activity)
            adapter =
                ProjectsAdapter(projects, object : ItemOnClickListener {
                    override fun onClickButton(position: Int, type: AboutType?, url: String?) {
                        navigateToProjectDetails(position)
                    }
                })
        }
    }

    private fun navigateToProjectDetails(position: Int) {
        val action = ProjectsFragmentDirections.actionProjectsFragmentToProjectDetailsFragment(position)
        findNavController().navigate(action)
    }
}