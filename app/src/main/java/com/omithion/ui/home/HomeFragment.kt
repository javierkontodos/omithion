package com.omithion.ui.home

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.omithion.R
import com.omithion.databinding.DialogCustomAlertBinding
import com.omithion.databinding.FragmentHomeBinding
import com.omithion.domain.HomeSlot
import com.omithion.ui.commons.visibleOrGone
import com.omithion.ui.commons.visibleOrNot
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.model.observe(viewLifecycleOwner) {
            when (it) {
                is HomeViewModel.UiModel.SuccessHomeSlots -> showHomeSlots(it.homeSlots)
                is HomeViewModel.UiModel.Failure -> showFailureError()
                is HomeViewModel.UiModel.Loading -> loading()
            }
        }

        viewModel.homeSlotsList?.let {
            showHomeSlots(it)
        }?: run {
            viewModel.getHomeContent()
        }

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in)
        binding.tvHomeSubTitle.startAnimation(anim)
        binding.ivFeatures.startAnimation(anim)
        binding.tvHomeTitleDescription.startAnimation(anim)

        val anim2 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_bounce)
        binding.tvHomeTitle.startAnimation(anim2)
        binding.tvHomeSecondTitle.startAnimation(anim2)
    }

    private fun loading() {
        Timber.d("Loading...")
        binding.loading.visibleOrGone(true)
    }

    private fun showFailureError() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity?.layoutInflater
        val dialogBinding: DialogCustomAlertBinding =
            DataBindingUtil.inflate(
                inflater!!,
                R.layout.dialog_custom_alert,
                null,
                false
            )
        builder.setView(dialogBinding.root)
        val alert: AlertDialog = builder.create()
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert.window?.setGravity(Gravity.CENTER)
        dialogBinding.btnClose.setOnClickListener {
            alert.dismiss()
        }
        dialogBinding.lyErrorDialog.visibleOrGone(true)
        alert.show()
    }

    private fun showHomeSlots(homeSlots: List<HomeSlot>) {
        binding.loading.visibleOrGone(false)
        with(binding.rvHomeSlots) {
            layoutManager = LinearLayoutManager(activity)
            adapter = HomeAdapter(homeSlots)
        }
        binding.svHomeContent.visibleOrNot(true)
    }
}