package com.omithion.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.omithion.data.usecases.GetHomeContentUseCase
import com.omithion.data.usecases.GetProjectsUseCase
import com.omithion.domain.HomeSlot
import com.omithion.domain.Project
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getHomeContentUseCase: GetHomeContentUseCase
) : ViewModel() {
    private var _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel> = _model
    var homeSlotsList: List<HomeSlot>? = null

    fun getHomeContent() {
        _model.value = UiModel.Loading
        viewModelScope.launch {
            getHomeContentUseCase.invoke()?.let {
                handleSuccess(it)
            } ?: handleFailure()
        }
    }

    private fun handleFailure() {
        _model.value = UiModel.Failure
    }

    private fun handleSuccess(homeSlots: List<HomeSlot>) {
        this.homeSlotsList = homeSlots
        _model.value = UiModel.SuccessHomeSlots(homeSlots)
    }

    sealed class UiModel {
        object Loading : UiModel()
        object Failure : UiModel()
        data class SuccessHomeSlots(val homeSlots: List<HomeSlot>) : UiModel()
    }
}