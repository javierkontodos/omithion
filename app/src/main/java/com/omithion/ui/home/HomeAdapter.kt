package com.omithion.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.omithion.R
import com.omithion.databinding.ItemHomeBinding
import com.omithion.domain.HomeSlot
import com.omithion.domain.Project
import com.omithion.ui.commons.ItemOnClickListener

class HomeAdapter (
    private val homeSlots: List<HomeSlot>
) : RecyclerView.Adapter<HomeViewHolder>() {

    override fun getItemCount(): Int {
        return homeSlots.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder =
        HomeViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_home,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.bind(homeSlots[position])
    }
}

class HomeViewHolder(val binding: ItemHomeBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(
        homeSlot: HomeSlot
    ) {
        binding.homeSlot = homeSlot

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_bounce)
        binding.tvTitle.startAnimation(anim)

        val anim2 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in)
        binding.ivHomeSlot.startAnimation(anim2)
        binding.tvParagraphOne.startAnimation(anim2)
        binding.tvParagraphTwo.startAnimation(anim2)
    }
}