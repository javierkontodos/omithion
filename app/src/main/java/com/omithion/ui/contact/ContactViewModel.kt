package com.omithion.ui.contact

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.omithion.data.usecases.SendContactUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContactViewModel @Inject constructor(
    private val sendContactUseCase: SendContactUseCase
) : ViewModel() {
    private var _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel> = _model

    fun sendContact(name: String, email: String, subject: String, query: String) {
        _model.value = UiModel.Loading
        viewModelScope.launch {
            if (sendContactUseCase.invoke(name, email, subject, query)) {
                handleSuccess()
            } else {
                handleFailure()
            }
        }
    }

    private fun handleFailure() {
        _model.value = UiModel.Failure
    }

    private fun handleSuccess() {
        _model.value = UiModel.Success
    }

    sealed class UiModel {
        object Loading : UiModel()
        object Failure : UiModel()
        object Success : UiModel()
    }
}