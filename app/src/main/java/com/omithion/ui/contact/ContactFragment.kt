package com.omithion.ui.contact

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.omithion.BuildConfig
import com.omithion.R
import com.omithion.databinding.DialogCustomAlertBinding
import com.omithion.databinding.FragmentContactBinding
import com.omithion.ui.commons.enableButton
import com.omithion.ui.commons.visibleOrGone
import com.omithion.ui.hideBottomNav
import com.omithion.ui.showBottomNav
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber


@AndroidEntryPoint
class ContactFragment : Fragment() {
    private lateinit var binding: FragmentContactBinding
    private val viewModel: ContactViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.model.observe(viewLifecycleOwner) {
            when (it) {
                is ContactViewModel.UiModel.Success -> showSuccessForm()
                is ContactViewModel.UiModel.Failure -> showFailureError()
                is ContactViewModel.UiModel.Loading -> loading()
            }
        }

        binding.root.keyboardListener {
            if (it) {
                this.hideBottomNav()
            } else {
                this.showBottomNav()
            }
        }

        loadMapsWebView()

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_fade_in)
        binding.tvAboutDescription.startAnimation(anim)
        binding.webViewMaps.startAnimation(anim)
        binding.cvContact.startAnimation(anim)

        val anim2 = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_bounce)
        binding.tvAboutTitle.startAnimation(anim2)
        binding.tvLocationTitle.startAnimation(anim2)
        binding.tvContactTitle.startAnimation(anim2)

        val version = "${requireContext().getString(R.string.contact_version)} ${BuildConfig.VERSION_NAME}"
        binding.tvVersion.text = version

        configForm()
    }

    private fun showSuccessForm() {
        binding.loading.visibleOrGone(false)

        binding.etName.setText("")
        binding.etEmail.setText("")
        binding.etSubject.setText("")
        binding.etQuery.setText("")


        val builder = AlertDialog.Builder(activity)
        val inflater = activity?.layoutInflater
        val dialogBinding: DialogCustomAlertBinding =
            DataBindingUtil.inflate(
                inflater!!,
                R.layout.dialog_custom_alert,
                null,
                false
            )
        builder.setView(dialogBinding.root)
        val alert: AlertDialog = builder.create()
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert.window?.setGravity(Gravity.CENTER)
        dialogBinding.btnClose.setOnClickListener {
            alert.dismiss()
        }
        dialogBinding.lyContactDialog.visibleOrGone(true)
        alert.show()
    }

    private fun loading() {
        Timber.d("Loading...")
        binding.loading.visibleOrGone(true)
    }

    private fun showFailureError() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity?.layoutInflater
        val dialogBinding: DialogCustomAlertBinding =
            DataBindingUtil.inflate(
                inflater!!,
                R.layout.dialog_custom_alert,
                null,
                false
            )
        builder.setView(dialogBinding.root)
        val alert: AlertDialog = builder.create()
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert.window?.setGravity(Gravity.CENTER)
        dialogBinding.btnClose.setOnClickListener {
            alert.dismiss()
        }
        dialogBinding.lyErrorDialog.visibleOrGone(true)
        alert.show()
    }

    private fun loadMapsWebView() {
        val iframe =
            "<iframe style=\"width: 100%;\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12154.769419124152!2d-3.696015315069501!3d40.39351053877445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x43fcb4596e5a2b5c!2sOmithion!5e0!3m2!1ses!2ses!4v1533409918986\" width=\"600\" height=\"200\" frameborder=\"0\" style=\"border:0\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>"
        binding.webViewMaps.settings.javaScriptEnabled = true
        binding.webViewMaps.loadData(iframe, "text/html", "utf-8")
    }

    private fun configForm() {

        val textObserver = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val name = binding.etName.text.toString()
                val email = binding.etEmail.text.toString()
                val subject = binding.etSubject.text.toString()
                val query = binding.etQuery.text.toString()

                if (name.isNotEmpty() && name.isNotBlank() && email.isNotEmpty() && email.isNotBlank() && email.isEmailValid() && subject.isNotEmpty() && subject.isNotBlank() && query.isNotEmpty() && query.isNotBlank()) {
                    binding.btnSend.enableButton(true)
                } else {
                    binding.btnSend.enableButton(false)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        }
        binding.etName.addTextChangedListener(textObserver)
        binding.etEmail.addTextChangedListener(textObserver)
        binding.etSubject.addTextChangedListener(textObserver)
        binding.etQuery.addTextChangedListener(textObserver)

        binding.btnSend.setOnClickListener {
            val name = binding.etName.text.toString()
            val email = binding.etEmail.text.toString()
            val subject = binding.etSubject.text.toString()
            val query = binding.etQuery.text.toString()

            viewModel.sendContact(name, email, subject, query)
        }
    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }
}

fun View.keyboardListener(listener: (Boolean) -> Unit) {
    this.viewTreeObserver.addOnGlobalLayoutListener {
        val r = Rect()
        this.getWindowVisibleDisplayFrame(r)
        val screenHeight = this.rootView.height
        val keypadHeight = screenHeight - r.bottom
        listener(keypadHeight > screenHeight * 0.15)
    }
}
