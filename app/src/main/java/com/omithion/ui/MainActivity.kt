package com.omithion.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.omithion.R
import com.omithion.databinding.ActivityMainBinding
import com.omithion.ui.commons.visibleOrGone
import com.omithion.ui.home.HomeFragmentDirections
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    companion object {
        private var notificationUrl: String? = null
        fun newIntent(
            context: Context,
            notificationUrl: String?
        ): Intent {
            this.notificationUrl = notificationUrl
            return Intent(context, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.transition_fade_in, R.anim.transition_fade_out)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val navController = findNavController(R.id.nav_host_fragment)
        binding.bottomNavigation.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.webViewFragment -> showFullScreen()
                R.id.projectDetailsFragment,
                R.id.aboutDetailsFragment -> showBackArrow()
                else -> hideFullScreen()
            }
        }

        binding.ivBack.setOnClickListener {
            navController.popBackStack()
        }

        notificationUrl?.let{
            var webUrl:String? = null
            val webPath = Uri.parse(notificationUrl).pathSegments

            if (webPath.size > 0){
                if (webPath[0] == "en_EN"){
                    if (webPath.size > 1){
                        webUrl = webPath[1]
                    }
                }else{
                    webUrl = webPath[0]
                }
            }

            webUrl?.let{
                when {
                    it.contains("projects") -> {
                        navigateToProjectsFragment(navController)
                    }
                    it.contains("about") -> {
                        navigateToAboutFragment(navController)
                    }
                    it.contains("contact") -> {
                        navigateToContactFragment(navController)
                    }
                    it.contains("educational") -> {
                        navigateToWebViewFragment(navController)
                    }
                }
            }
        }
    }

    private fun navigateToProjectsFragment(navController: NavController) {
        val action = HomeFragmentDirections.actionHomeFragmentToProjectsFragment()
        navController.navigate(action)
    }

    private fun navigateToAboutFragment(navController: NavController) {
        val action = HomeFragmentDirections.actionHomeFragmentToAboutFragment()
        navController.navigate(action)
    }

    private fun navigateToContactFragment(navController: NavController) {
        val action = HomeFragmentDirections.actionHomeFragmentToContactFragment()
        navController.navigate(action)
    }

    private fun navigateToWebViewFragment(navController: NavController) {
        val action = HomeFragmentDirections.actionHomeFragmentToWebViewFragment("https://omithion.com/educational")
        navController.navigate(action)
    }

    private fun showFullScreen() {
        binding.bottomNavigation.visibleOrGone(false)
        binding.collapsingToolbar.visibleOrGone(false)
        binding.ivBack.visibleOrGone(false)
    }

    private fun hideFullScreen() {
        binding.bottomNavigation.visibleOrGone(true)
        binding.collapsingToolbar.visibleOrGone(true)
        binding.ivBack.visibleOrGone(false)
        binding.appbarMain.setExpanded(true, true)

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_zoom_in_out)
        binding.ivLogo.startAnimation(anim)
    }

    private fun showBackArrow() {
        binding.ivBack.visibleOrGone(true)
        binding.bottomNavigation.visibleOrGone(false)
        binding.collapsingToolbar.visibleOrGone(true)
        binding.appbarMain.setExpanded(true, true)

        val anim = AnimationUtils.loadAnimation(binding.root.context, R.anim.anim_zoom_in_out)
        binding.ivLogo.startAnimation(anim)
    }

    fun showBottomNav() {
        binding.bottomNavigation.visibleOrGone(true)
    }

    fun hideBottomNav() {
        binding.bottomNavigation.visibleOrGone(false)
    }
}

fun Fragment.showBottomNav() {
    if (this.activity is MainActivity) {
        (this.activity as MainActivity).showBottomNav()
    }
}

fun Fragment.hideBottomNav() {
    if (this.activity is MainActivity) {
        (this.activity as MainActivity).hideBottomNav()
    }
}
