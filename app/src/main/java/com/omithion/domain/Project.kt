package com.omithion.domain

data class Project(
    val name: String,
    val description: String,
    val image: String,
    val url: String
)