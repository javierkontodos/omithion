package com.omithion.domain

data class About(
    val name: String,
    val title: String,
    val description: String,
    val image: String,
    val links: List<AboutLink>
)

data class AboutLink(
    val type: AboutType,
    val url: String
)

enum class AboutType {
    Linkedin, Instagram, Twitter, Telegram, Email, Website, Other
}