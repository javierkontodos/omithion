package com.omithion.domain

data class HomeSlot(
    val title: String,
    val image: String,
    val paragraphOne: String,
    val paragraphTwo: String
)