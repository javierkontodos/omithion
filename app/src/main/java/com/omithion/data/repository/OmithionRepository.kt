package com.omithion.data.repository

import com.omithion.data.source.RemoteDataSource
import com.omithion.data.toDomainAboutList
import com.omithion.data.toDomainHomeSlotList
import com.omithion.data.toDomainProjectList
import com.omithion.domain.About
import com.omithion.domain.HomeSlot
import com.omithion.domain.Project
import java.util.*
import javax.inject.Inject

class OmithionRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) {

    suspend fun getHomeContent(): List<HomeSlot>? {
        val response = remoteDataSource.getHomeContent(getLocale())

        return if (response != null){
            if (response.error != null){
                null
            }else{
                response.slots.toDomainHomeSlotList()
            }
        }else{
            null
        }
    }

    suspend fun getProjects(): List<Project>? {
        val response = remoteDataSource.getProjects(getLocale())

        return if (response != null){
            if (response.error != null){
                null
            }else{
                response.data.toDomainProjectList()
            }
        }else{
            null
        }
    }

    suspend fun getAbout(): List<About>? {
        val response = remoteDataSource.getAbout(getLocale())

        return if (response != null){
            if (response.error != null){
                null
            }else{
                response.data.toDomainAboutList()
            }
        }else{
            null
        }
    }

    suspend fun sendContact(name: String, email: String, subject: String, query: String): Boolean {
        val response = remoteDataSource.sendContact(getLocale(), name, email, subject, query)
        var isSuccess = false
        if (response != null) {
            if (response.success != null) {
                isSuccess = true
            }
        }
        return isSuccess
    }

    private fun getLocale(): String =
        when (Locale.getDefault().language) {
            "es" -> "es_ES"
            "en" -> "en_EN"
            else -> "es_ES"
        }
}