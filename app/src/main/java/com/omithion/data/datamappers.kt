package com.omithion.data

import com.omithion.data.server.response.*
import com.omithion.domain.*


fun List<HomeContentSlotResponse>.toDomainHomeSlotList(): List<HomeSlot> =
    map { it.toDomainHomeSlot() }

fun HomeContentSlotResponse.toDomainHomeSlot(): HomeSlot =
    HomeSlot(this.title ?: "", this.image ?: "", this.paragraphOne ?: "", this.paragraphTwo ?: "")

fun List<ProjectDataResponse>.toDomainProjectList(): List<Project> =
    map { it.toDomainProject() }

fun ProjectDataResponse.toDomainProject(): Project =
    Project(this.name ?: "", this.content ?: "", this.image ?: "", this.url ?: "")

fun List<AboutDataResponse>.toDomainAboutList(): List<About> =
    map { it.toDomainAbout() }

fun AboutDataResponse.toDomainAbout(): About =
    About(
        this.name ?: "",
        this.title ?: "",
        this.content ?: "",
        this.image ?: "",
        this.links.toDomainAboutLinks()
    )

private fun List<AboutLinkResponse>?.toDomainAboutLinks(): List<AboutLink> =
    this?.map {
        AboutLink(it.toDomainAboutType(), it.url ?: "")
    } ?: listOf()

private fun AboutLinkResponse.toDomainAboutType(): AboutType =
    when (this.name) {
        "linkedin" -> AboutType.Linkedin
        "instagram" -> AboutType.Instagram
        "twitter" -> AboutType.Twitter
        "telegram" -> AboutType.Telegram
        "email" -> AboutType.Email
        "website" -> AboutType.Website
        else -> AboutType.Other
    }