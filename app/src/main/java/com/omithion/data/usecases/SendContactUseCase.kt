package com.omithion.data.usecases

import com.omithion.data.repository.OmithionRepository
import com.omithion.domain.About
import javax.inject.Inject

class SendContactUseCase @Inject constructor(
    private val omithionRepository: OmithionRepository
) {
    suspend fun invoke(name: String, email: String, subject: String, query: String) =
        omithionRepository.sendContact(name, email, subject, query)
}