package com.omithion.data.usecases

import com.omithion.data.repository.OmithionRepository
import com.omithion.domain.Project
import javax.inject.Inject

class GetProjectsUseCase @Inject constructor(
    private val omithionRepository: OmithionRepository
) {
    suspend fun invoke(): List<Project>? =
        omithionRepository.getProjects()
}