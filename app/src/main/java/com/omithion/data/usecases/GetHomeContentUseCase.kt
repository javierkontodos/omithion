package com.omithion.data.usecases

import com.omithion.data.repository.OmithionRepository
import com.omithion.domain.HomeSlot
import javax.inject.Inject

class GetHomeContentUseCase @Inject constructor(
    private val omithionRepository: OmithionRepository
) {
    suspend fun invoke(): List<HomeSlot>? =
        omithionRepository.getHomeContent()
}