package com.omithion.data.usecases

import com.omithion.data.repository.OmithionRepository
import com.omithion.domain.About
import javax.inject.Inject

class GetAboutUseCase @Inject constructor(
    private val omithionRepository: OmithionRepository
) {
    suspend fun invoke(): List<About>? =
        omithionRepository.getAbout()
}