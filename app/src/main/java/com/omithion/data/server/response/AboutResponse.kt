package com.omithion.data.server.response

import com.google.gson.annotations.SerializedName

data class AboutResponse (
    @SerializedName("title")
    val title: String,
    @SerializedName("data")
    val data: List<AboutDataResponse>,
    @SerializedName("error")
    val error: String?
)

data class AboutDataResponse (
    @SerializedName("image")
    val image: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("content")
    val content: String?,
    @SerializedName("links")
    val links: List<AboutLinkResponse>?
)

data class AboutLinkResponse (
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)