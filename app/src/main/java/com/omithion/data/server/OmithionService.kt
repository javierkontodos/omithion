package com.omithion.data.server

import com.omithion.data.server.response.AboutResponse
import com.omithion.data.server.response.ContactResponse
import com.omithion.data.server.response.HomeContentResponse
import com.omithion.data.server.response.ProjectResponse
import okhttp3.RequestBody
import retrofit2.http.*

interface OmithionService {
    @GET("home")
    @Headers("Accept: application/json")
    suspend fun getHomeContent(
        @Query("locale") locale: String
    ): HomeContentResponse

    @GET("projects")
    @Headers("Accept: application/json")
    suspend fun getProjects(
        @Query("locale") locale: String
    ): ProjectResponse

    @GET("about")
    @Headers("Accept: application/json")
    suspend fun getAbout(
        @Query("locale") locale: String
    ): AboutResponse

    @POST("contact")
    suspend fun sendContact(
        @Query("locale") locale: String,
        @Body body: RequestBody
    ): ContactResponse
}