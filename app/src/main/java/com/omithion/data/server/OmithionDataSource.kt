package com.omithion.data.server

import com.omithion.data.server.response.AboutResponse
import com.omithion.data.server.response.ContactResponse
import com.omithion.data.server.response.HomeContentResponse
import com.omithion.data.server.response.ProjectResponse
import com.omithion.data.source.RemoteDataSource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Retrofit
import timber.log.Timber
import javax.inject.Inject


class OmithionDataSource @Inject constructor(
    private val omithionService: OmithionService
) : RemoteDataSource {

    override suspend fun getHomeContent(locale: String): HomeContentResponse? = try {
        omithionService.getHomeContent(locale)
    } catch (exception: Exception) {
        Timber.e("ApiError: ${exception.localizedMessage}")
        null
    }

    override suspend fun getProjects(locale: String): ProjectResponse? = try {
        omithionService.getProjects(locale)
    } catch (exception: Exception) {
        Timber.e("ApiError: ${exception.localizedMessage}")
        null
    }

    override suspend fun getAbout(locale: String): AboutResponse? = try {
        omithionService.getAbout(locale)
    } catch (exception: Exception) {
        Timber.e("ApiError: ${exception.localizedMessage}")
        null
    }

    override suspend fun sendContact(
        locale: String,
        name: String,
        email: String,
        subject: String,
        query: String
    ): ContactResponse? = try {
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("name", name)
            .addFormDataPart("email", email)
            .addFormDataPart("subject", subject)
            .addFormDataPart("query", query)
            .build()
        omithionService.sendContact(locale, requestBody)
    } catch (exception: Exception) {
        Timber.e("ApiError: ${exception.localizedMessage}")
        null
    }
}