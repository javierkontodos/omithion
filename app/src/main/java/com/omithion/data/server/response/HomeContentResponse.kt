package com.omithion.data.server.response

import com.google.gson.annotations.SerializedName

data class HomeContentResponse (
    @SerializedName("title")
    val title: String,
    @SerializedName("slots")
    val slots: List<HomeContentSlotResponse>,
    @SerializedName("error")
    val error: String?
)

data class HomeContentSlotResponse (
    @SerializedName("title")
    val title: String?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("paragraph_one")
    val paragraphOne: String?,
    @SerializedName("paragraph_two")
    val paragraphTwo: String?
)