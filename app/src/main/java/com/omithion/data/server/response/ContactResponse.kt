package com.omithion.data.server.response

import com.google.gson.annotations.SerializedName

data class ContactResponse (
    @SerializedName("success")
    val success: String?,
    @SerializedName("error")
    val error: String?
)