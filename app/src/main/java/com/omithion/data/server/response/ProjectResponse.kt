package com.omithion.data.server.response

import com.google.gson.annotations.SerializedName

data class ProjectResponse (
    @SerializedName("title")
    val title: String,
    @SerializedName("data")
    val data: List<ProjectDataResponse>,
    @SerializedName("error")
    val error: String?
)

data class ProjectDataResponse (
    @SerializedName("image")
    val image: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("content")
    val content: String?,
    @SerializedName("url")
    val url: String?
)