package com.omithion.data.di

import com.omithion.data.server.OmithionDataSource
import com.omithion.data.source.RemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DataModule {
}

@Module
@InstallIn(SingletonComponent::class)
abstract class BindsDataModule {
/*
    @Binds
    abstract fun bindsLocalDataSource(sharedPreferencesDataSource: SharedPreferencesDataSource): LocalDataSource
*/

    @Binds
    abstract fun bindRemoteDataSource(goliathDataSource: OmithionDataSource): RemoteDataSource
}