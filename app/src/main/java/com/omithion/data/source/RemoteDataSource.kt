package com.omithion.data.source

import com.omithion.data.server.response.AboutResponse
import com.omithion.data.server.response.ContactResponse
import com.omithion.data.server.response.HomeContentResponse
import com.omithion.data.server.response.ProjectResponse

interface RemoteDataSource {
    suspend fun getHomeContent(locale: String): HomeContentResponse?
    suspend fun getProjects(locale: String): ProjectResponse?
    suspend fun getAbout(locale: String): AboutResponse?
    suspend fun sendContact(locale: String, name: String, email: String, subject: String, query: String): ContactResponse?
}